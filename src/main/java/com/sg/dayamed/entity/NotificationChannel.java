package com.sg.dayamed.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

/**
 * Created by Eresh Gorantla on 31/May/2019
 **/
@Entity
@Table(name = "notification_channel")
@Getter
@Setter
@NoArgsConstructor
@DynamicUpdate
@DynamicInsert
public class NotificationChannel {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "created_date", updatable = false)
    private LocalDateTime createdDate;
}
