package com.sg.dayamed.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import org.hibernate.annotations.ColumnTransformer;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.ZonedDateTime;

/**
 * Created By Gorantla, Eresh on 23/Nov/2019
 **/
@Entity
@Table(name = "notification_result")
@Getter
@Setter
@NoArgsConstructor
public class NotificationResult {

	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "org.hibernate.id.UUIDGenerator")
	private String id;

	@Column(name = "type")
	private String type;

	@Column(name = "channel")
	private String channel;

	@Column(name = "value", columnDefinition = "VARBINARY")
	@ColumnTransformer(forColumn = "value", read = "CAST(AES_DECRYPT(value, DB_ENCRYPTION_PASSWORD()) AS CHAR(250))", write = "AES_ENCRYPT(?, " +
			"DB_ENCRYPTION_PASSWORD())")
	private String value;

	@Column(name = "result")
	private String result;

	@Column(name = "stacktrace", columnDefinition = "text")
	private String stackTrace;

	@Column(name = "created_date", updatable = false)
	private ZonedDateTime createdDate = ZonedDateTime.now();
}

