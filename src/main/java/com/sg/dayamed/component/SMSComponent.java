package com.sg.dayamed.component;

import com.sg.dayamed.component.vo.MessageVO;
import com.sg.dayamed.crypto.SecurityUtil;
import com.sg.dayamed.exceptions.ExceptionLogUtil;
import com.sg.dayamed.component.vo.MessageModelVO;
import com.sg.dayamed.util.SensitiveDataMaskingUtility;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Created By Gorantla, Eresh on 23/Nov/2019
 **/
@Component
@Scope("prototype")
public class SMSComponent extends BaseNotificationComponent {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	String accountSid = null;

	String authToken = null;

	String fromContact = null;

	@Autowired
	SecretsComponent secretsComponent;

	public void init() throws Exception {
		this.accountSid = SecurityUtil.decryptText(secretsComponent.getTwilioAccountSID());
		this.authToken = SecurityUtil.decryptText(secretsComponent.getTwilioAuthToken());
		this.fromContact = SecurityUtil.decryptText(secretsComponent.getTwilioFromContact());
		Twilio.init(accountSid, authToken);
	}

	public MessageVO sendSMSNotification(MessageVO messageVO) {
		Exception exception = null;
		try {
			MessageModelVO messageModelVO = (MessageModelVO) messageVO;
			Message.creator(new PhoneNumber(messageModelVO.getContact()), new PhoneNumber(fromContact), messageModelVO.getMessage())
			       .create();
			logger.info("SMS is Successfully delivered to -> " + SensitiveDataMaskingUtility.maskMobileNumber(messageModelVO.getContact()));
		} catch (Exception e) {
			ExceptionLogUtil.log(e, "Exception while sending sms", logger);
			exception = e;
		}
		messageVO.setException(exception);
		return messageVO;
	}
}
