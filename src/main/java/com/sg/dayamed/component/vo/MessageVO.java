package com.sg.dayamed.component.vo;

import com.sg.dayamed.util.enums.NotificationChannel;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created By Gorantla, Eresh on 24/Nov/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class MessageVO {
	protected String notificationType;
	protected NotificationChannel channel;
	protected Exception exception;
}
