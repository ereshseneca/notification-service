package com.sg.dayamed.component.vo;

import com.sg.dayamed.service.vo.NotificationVO;
import com.sg.dayamed.util.enums.NotificationChannel;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created By Gorantla, Eresh on 23/Nov/2019
 **/
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EmailMessageVO extends MessageVO {

	private List<String> emailIds = new ArrayList<>();

	private String content;

	private String subject;

	public EmailMessageVO(NotificationVO notificationVO, String content, String subject) {
		if (StringUtils.isNotBlank(content)) {
			this.emailIds = Stream.of(notificationVO.getValue()).collect(Collectors.toList());
			this.content = content;
			this.subject = subject;
			this.notificationType = notificationVO.getNotificationType();
			this.channel = NotificationChannel.EMAIL;
			this.exception = null;
		}
	}
}
