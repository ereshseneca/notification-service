package com.sg.dayamed.component.vo;

import com.sg.dayamed.service.vo.NotificationVO;
import com.sg.dayamed.util.enums.NotificationChannel;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import org.apache.commons.lang3.StringUtils;

/**
 * Created By Gorantla, Eresh on 23/Nov/2019
 **/
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MessageModelVO extends MessageVO {

	private String contact;

	private String message;

	public MessageModelVO(NotificationVO notificationVO, String content) {
		if (StringUtils.isNotBlank(content)) {
			this.contact = notificationVO.getValue();
			this.message = content;
			this.notificationType = notificationVO.getNotificationType();
			this.channel = NotificationChannel.SMS;
			this.exception = null;
		}
	}

}
