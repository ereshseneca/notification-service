package com.sg.dayamed.component;

import com.sg.dayamed.redis.NotificationKeysEnum;
import com.sg.dayamed.redis.RedisConstants;
import com.sg.dayamed.redis.RedisService;
import com.sg.dayamed.service.vo.MatchingPatternVO;
import com.sg.dayamed.util.enums.NotificationChannel;
import com.sg.dayamed.util.enums.NotificationType;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created By Gorantla, Eresh on 23/Nov/2019
 **/
@Component
public class NotificationTemplateUtils {

	public static final String DEFAULT_LANGUAGE = "en";

	public static final String SPANISH_LANGUAGE = "es";

	public static final MatchingPatternVO NEW_LINE_REGULAR_EXPRESSION = new MatchingPatternVO(":::", System.lineSeparator());

	private List<MatchingPatternVO> matchingPatterns = new ArrayList<>();

	@Autowired
	RedisService redisService;

	@PostConstruct
	public void init() {
		this.matchingPatterns = Stream.of(NEW_LINE_REGULAR_EXPRESSION)
		                              .collect(Collectors.toList());
	}

	public String getUpdateProfileEmailContent(String langKey) {
		String data;
		if (DEFAULT_LANGUAGE.equalsIgnoreCase(langKey)) {
			return (String) redisService.getDataInRedisByKey(RedisConstants.UPDATE_PROFILE_EMAIL_TEMPLATE_EN);
		} else if (SPANISH_LANGUAGE.equalsIgnoreCase(langKey)) {
			data = (String) redisService.getDataInRedisByKey(RedisConstants.UPDATE_PROFILE_EMAIL_TEMPLATE_ES);
			return StringUtils.isNotBlank(data) ? data :
					(String) redisService.getDataInRedisByKey(RedisConstants.UPDATE_PROFILE_EMAIL_TEMPLATE_EN);
		}
		return (String) redisService.getDataInRedisByKey(RedisConstants.UPDATE_PROFILE_EMAIL_TEMPLATE_EN);
	}

	public String getUpdateProfileSmsContent(String langKey) {
		String data;
		if (DEFAULT_LANGUAGE.equalsIgnoreCase(langKey)) {
			data = (String) redisService.getDataInRedisByKey(RedisConstants.UPDATE_PROFILE_SMS_TEMPLATE_EN);
			return replaceMatchingPatterns(data);
		} else if (SPANISH_LANGUAGE.equalsIgnoreCase(langKey)) {
			data = (String) redisService.getDataInRedisByKey(RedisConstants.UPDATE_PROFILE_SMS_TEMPLATE_ES);
			return StringUtils.isNotBlank(data) ? replaceMatchingPatterns(data) :
					replaceMatchingPatterns((String) redisService.getDataInRedisByKey(RedisConstants.UPDATE_PROFILE_SMS_TEMPLATE_EN));
		}
		data = (String) redisService.getDataInRedisByKey(RedisConstants.UPDATE_PROFILE_SMS_TEMPLATE_EN);
		return replaceMatchingPatterns(data);
	}

	public String getUpdateProfileEmailSubject(String langKey) {
		String data;
		if (DEFAULT_LANGUAGE.equalsIgnoreCase(langKey)) {
			return (String) redisService.getDataInRedisByKey(RedisConstants.UPDATE_PROFILE_EMAIL_SUBJECT_EN);
		} else if (SPANISH_LANGUAGE.equalsIgnoreCase(langKey)) {
			data = (String) redisService.getDataInRedisByKey(RedisConstants.UPDATE_PROFILE_EMAIL_SUBJECT_ES);
			return StringUtils.isNotBlank(data) ? data :
					(String) redisService.getDataInRedisByKey(RedisConstants.UPDATE_PROFILE_EMAIL_SUBJECT_EN);
		}
		return (String) redisService.getDataInRedisByKey(RedisConstants.UPDATE_PROFILE_EMAIL_SUBJECT_EN);
	}

	public String getResetPasswordEmailSubject(String langKey) {
		String data;
		if (DEFAULT_LANGUAGE.equalsIgnoreCase(langKey)) {
			return (String) redisService.getDataInRedisByKey(RedisConstants.RESET_PASSWORD_EMAIL_SUBJECT_EN);
		} else if (SPANISH_LANGUAGE.equalsIgnoreCase(langKey)) {
			data = (String) redisService.getDataInRedisByKey(RedisConstants.RESET_PASSWORD_EMAIL_SUBJECT_EN);
			return StringUtils.isNotBlank(data) ? data :
					(String) redisService.getDataInRedisByKey(RedisConstants.RESET_PASSWORD_EMAIL_SUBJECT_EN);
		}
		return (String) redisService.getDataInRedisByKey(RedisConstants.RESET_PASSWORD_EMAIL_SUBJECT_EN);
	}

	public String getResetPasswordEmailContent(String langKey) {
		String data;
		if (DEFAULT_LANGUAGE.equalsIgnoreCase(langKey)) {
			return (String) redisService.getDataInRedisByKey(RedisConstants.RESET_PASSWORD_EMAIL_TEMPLATE_EN);
		} else if (SPANISH_LANGUAGE.equalsIgnoreCase(langKey)) {
			data = (String) redisService.getDataInRedisByKey(RedisConstants.RESET_PASSWORD_EMAIL_TEMPLATE_EN);
			return StringUtils.isNotBlank(data) ? data :
					(String) redisService.getDataInRedisByKey(RedisConstants.RESET_PASSWORD_EMAIL_TEMPLATE_EN);
		}
		return (String) redisService.getDataInRedisByKey(RedisConstants.RESET_PASSWORD_EMAIL_TEMPLATE_EN);
	}

	public String getResetPasswordSmsContent(String langKey) {
		String data;
		if (DEFAULT_LANGUAGE.equalsIgnoreCase(langKey)) {
			data = (String) redisService.getDataInRedisByKey(RedisConstants.RESET_PASSWORD_SMS_TEMPLATE_EN);
			return replaceMatchingPatterns(data);
		} else if (SPANISH_LANGUAGE.equalsIgnoreCase(langKey)) {
			data = (String) redisService.getDataInRedisByKey(RedisConstants.RESET_PASSWORD_SMS_TEMPLATE_EN);
			return StringUtils.isNotBlank(data) ? replaceMatchingPatterns(data) :
					replaceMatchingPatterns((String) redisService.getDataInRedisByKey(RedisConstants.RESET_PASSWORD_SMS_TEMPLATE_EN));
		}
		data = (String) redisService.getDataInRedisByKey(RedisConstants.RESET_PASSWORD_SMS_TEMPLATE_EN);
		return replaceMatchingPatterns(data);
	}

	public String getUpdatePrescriptionEmailSubject(String langKey) {
		String data;
		if (DEFAULT_LANGUAGE.equalsIgnoreCase(langKey)) {
			return (String) redisService.getDataInRedisByKey(RedisConstants.PRESCRIPTION_UPDATE_EMAIL_SUBJECT_EN);
		} else if (SPANISH_LANGUAGE.equalsIgnoreCase(langKey)) {
			data = (String) redisService.getDataInRedisByKey(RedisConstants.PRESCRIPTION_UPDATE_EMAIL_SUBJECT_ES);
			return StringUtils.isNotBlank(data) ? data :
					(String) redisService.getDataInRedisByKey(RedisConstants.PRESCRIPTION_UPDATE_EMAIL_SUBJECT_EN);
		}
		return (String) redisService.getDataInRedisByKey(RedisConstants.PRESCRIPTION_UPDATE_EMAIL_SUBJECT_EN);
	}

	public String getUpdatePrescriptionEmailContent(String langKey) {
		String data;
		if (DEFAULT_LANGUAGE.equalsIgnoreCase(langKey)) {
			return (String) redisService.getDataInRedisByKey(RedisConstants.PRESCRIPTION_UPDATE_EMAIL_TEMPLATE_EN);
		} else if (SPANISH_LANGUAGE.equalsIgnoreCase(langKey)) {
			data = (String) redisService.getDataInRedisByKey(RedisConstants.PRESCRIPTION_UPDATE_EMAIL_TEMPLATE_ES);
			return StringUtils.isNotBlank(data) ? data :
					(String) redisService.getDataInRedisByKey(RedisConstants.PRESCRIPTION_UPDATE_EMAIL_TEMPLATE_EN);
		}
		return (String) redisService.getDataInRedisByKey(RedisConstants.PRESCRIPTION_UPDATE_EMAIL_TEMPLATE_EN);
	}

	public String getUpdatePrescriptionSmsContent(String langKey) {
		String data;
		if (DEFAULT_LANGUAGE.equalsIgnoreCase(langKey)) {
			data = (String) redisService.getDataInRedisByKey(RedisConstants.PRESCRIPTION_UPDATE_SMS_TEMPLATE_EN);
			return replaceMatchingPatterns(data);
		} else if (SPANISH_LANGUAGE.equalsIgnoreCase(langKey)) {
			data = (String) redisService.getDataInRedisByKey(RedisConstants.PRESCRIPTION_UPDATE_SMS_TEMPLATE_ES);
			return StringUtils.isNotBlank(data) ? replaceMatchingPatterns(data) :
					replaceMatchingPatterns((String) redisService.getDataInRedisByKey(RedisConstants.PRESCRIPTION_UPDATE_SMS_TEMPLATE_EN));
		}
		data = (String) redisService.getDataInRedisByKey(RedisConstants.PRESCRIPTION_UPDATE_SMS_TEMPLATE_EN);
		return replaceMatchingPatterns(data);
	}

	public String getProductName() {
		return (String) redisService.getDataInRedisByKey(RedisConstants.PRODUCT_NAME);
	}

	private String replaceMatchingPatterns(String data) {

		for (MatchingPatternVO patternVO : matchingPatterns) {
			data = data.replaceAll(patternVO.getKey(), patternVO.getValue());
		}
		return data;
	}

}
