package com.sg.dayamed.component;

import com.sg.dayamed.redis.RedisConstants;
import com.sg.dayamed.redis.RedisService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created By Gorantla, Eresh on 23/Nov/2019
 **/
@Component
public class SecretsComponent {

	@Autowired
	RedisService redisService;

	public String getTwilioAccountSID() {
		return (String) redisService.getDataInRedisByKey(RedisConstants.TWILIO_ACCOUNT_SID);
	}

	public String getTwilioAuthToken() {
		return (String) redisService.getDataInRedisByKey(RedisConstants.TWILIO_AUTH_TOKEN);
	}

	public String getTwilioFromContact() {
		return (String) redisService.getDataInRedisByKey(RedisConstants.TWILIO_FROM_CONTACT);
	}
}
