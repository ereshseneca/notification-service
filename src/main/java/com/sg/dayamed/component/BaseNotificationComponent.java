package com.sg.dayamed.component;

import com.sg.dayamed.component.vo.EmailMessageVO;
import com.sg.dayamed.component.vo.MessageModelVO;
import com.sg.dayamed.component.vo.MessageVO;
import com.sg.dayamed.exceptions.ExceptionLogUtil;
import com.sg.dayamed.service.NotificationResultService;
import com.sg.dayamed.service.vo.NotificationResultVO;
import com.sg.dayamed.util.enums.NotificationChannel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;

/**
 * Created By Gorantla, Eresh on 24/Nov/2019
 **/
public class BaseNotificationComponent {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	NotificationResultService notificationResultService;

	@Autowired
	@Qualifier("notificationResult")
	ExecutorService notificationResultExecutorService;


	public void sendNotification(MessageVO messageVO) {
		if (NotificationChannel.EMAIL.equals(messageVO.getChannel())) {
			messageVO = getEmailComponent().sendEmail(messageVO);
		}
		if (NotificationChannel.SMS.equals(messageVO.getChannel())) {
			messageVO = getSmsComponent().sendSMSNotification(messageVO);
		}
		MessageVO finalMessageVO = messageVO;
		CompletableFuture.runAsync(() -> saveNotificationResult(finalMessageVO), notificationResultExecutorService);
	}

	public void saveNotificationResult(MessageVO messageVO) {
		NotificationResultVO resultVO = new NotificationResultVO();
		try {
			if (NotificationChannel.EMAIL.equals(messageVO.getChannel())) {
				EmailMessageVO emailMessageVO = (EmailMessageVO) messageVO;
				resultVO = new NotificationResultVO(emailMessageVO);
			}
			if (NotificationChannel.SMS.equals(messageVO.getChannel())) {
				MessageModelVO messageModelVO = (MessageModelVO) messageVO;
				resultVO = new NotificationResultVO(messageModelVO);
			}
			notificationResultService.saveNotificationResult(resultVO);
		} catch (Exception e) {
			ExceptionLogUtil.log(e, "Exception in saving notification result", logger);
		}
	}

	@Lookup
	public EmailComponent getEmailComponent() {
		return null;
	}

	@Lookup
	public SMSComponent getSmsComponent() {
		return null;
	}
}
