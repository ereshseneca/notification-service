package com.sg.dayamed.queues;

import com.sg.dayamed.config.ActiveMQConfiguration;
import com.sg.dayamed.exceptions.ApplicationException;
import com.sg.dayamed.component.vo.MessageModelVO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

/**
 * Created By Gorantla, Eresh on 23/Nov/2019
 **/
@Component
public class SmsSender {

	@Autowired
	JmsTemplate jmsTemplate;

	public void sendSMS(MessageModelVO modelVO) throws ApplicationException {
		try {
			if (modelVO != null) {
				jmsTemplate.convertAndSend(ActiveMQConfiguration.SMS_QUEUE, modelVO);
			}
		} catch (Exception e) {
			throw new ApplicationException(e);
		}
	}
}
