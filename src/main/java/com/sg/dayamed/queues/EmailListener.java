package com.sg.dayamed.queues;

import com.sg.dayamed.component.EmailComponent;
import com.sg.dayamed.config.ActiveMQConfiguration;
import com.sg.dayamed.exceptions.ExceptionLogUtil;
import com.sg.dayamed.component.vo.EmailMessageVO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

/**
 * Created By Gorantla, Eresh on 23/Nov/2019
 **/
@Component
public class EmailListener {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	EmailComponent emailComponent;

	@JmsListener(destination = ActiveMQConfiguration.EMAIL_QUEUE)
	public void processEmail(@Payload EmailMessageVO messageVO, @Headers MessageHeaders messageHeaders, Message message) {
		try {
			emailComponent.sendNotification(messageVO);
		} catch (Exception e) {
			ExceptionLogUtil.log(e, "Exception while processing in queue", logger);
		}
	}
}
