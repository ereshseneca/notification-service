package com.sg.dayamed.queues;

import com.sg.dayamed.component.SMSComponent;
import com.sg.dayamed.config.ActiveMQConfiguration;
import com.sg.dayamed.component.vo.MessageModelVO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

/**
 * Created By Gorantla, Eresh on 23/Nov/2019
 **/
@Component
public class SmsListener {

	@Autowired
	SMSComponent smsComponent;

	@JmsListener(destination = ActiveMQConfiguration.SMS_QUEUE)
	public void processSMS(@Payload MessageModelVO messageVO, @Headers MessageHeaders messageHeaders, Message message) {
		smsComponent.sendNotification(messageVO);
	}
}
