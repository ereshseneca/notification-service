package com.sg.dayamed.queues;

import com.sg.dayamed.config.ActiveMQConfiguration;
import com.sg.dayamed.exceptions.ApplicationException;
import com.sg.dayamed.component.vo.EmailMessageVO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

/**
 * Created By Gorantla, Eresh on 23/Nov/2019
 **/
@Component
public class EmailSender {

	@Autowired
	JmsTemplate jmsTemplate;

	public void sendEmail(EmailMessageVO messageVO) throws ApplicationException {
		try {
			if (messageVO != null) {
				jmsTemplate.convertAndSend(ActiveMQConfiguration.EMAIL_QUEUE, messageVO);
			}
		} catch (Exception e) {
			throw new ApplicationException(e);
		}
	}
}
