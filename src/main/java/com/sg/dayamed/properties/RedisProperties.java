package com.sg.dayamed.properties;

import lombok.Getter;
import lombok.Setter;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Created By Gorantla, Eresh on 23/Nov/2019
 **/
@Configuration
@ConfigurationProperties(prefix = "dayamed.cache.redis")
@Getter
@Setter
public class RedisProperties {

	private String host;

	private Integer port;

	private String password;

	private String jmsPrefix;

	private Integer maxIdle;

	private Integer minIdle;

	private Integer maxTotal;

	private Integer maxWaitMillis;

	private Boolean testOnBorrow;

	private Integer numTestsPerEvictionRun;
}
