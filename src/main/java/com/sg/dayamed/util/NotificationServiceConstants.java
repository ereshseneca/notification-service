package com.sg.dayamed.util;

/**
 * Created By Gorantla, Eresh on 23/Nov/2019
 **/

public interface NotificationServiceConstants {
	String SUCCESS_STATUS = "Success";
	String FAILURE_STATUS = "Failure";
}
