package com.sg.dayamed.util.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Created By Gorantla, Eresh on 23/Nov/2019
 **/
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum NotificationType {
	RESET_PASSWORD(1, "Reset Password"),
	PRESCRIPTION_NOTIFICATIONS(2, "Prescription Notifications"),
	ADHERENCE_EVENT_NOTIFICATIONS(3, "Adherence Events Notifications"),
	CUSTOM_DOSAGE_NOTIFICATION(4, "Custom Dosage Notifications"),
	PATIENT_DOSAGE_SCHEDULE(5, "Patient Dosage Schedule"),
	NON_PATIENT_DOSAGE_SCHEDULE(6, "Dosage Schedule to Patient Associations"),
	PATIENT_SECOND_ALERT_NOTIFICATIONS(7, "Patient Second Alert Notifications"),
	PRESCRIPTION_UPDATE_NOTIFICATION(8, "Prescription Update Notifications"),
	ADHERENCE_PATIENT_MEDICATION_STATUS(9, "Patient Adherence Medication Status"),
	ADHERENCE_PATIENT_ASSOCIATION_MEDICATION_STATUS(9, "Patient Adherence Association Medication Status"),
	DEVICE_INFO_SCHEDULE_PATIENT_NOTIFICATIONS(10, "Device Info Schedule Patient Notifications"),
	DEVICE_INFO_SCHEDULE_PATIENT_ASSOCIATIONS_NOTIFICATIONS(11, "Device Info Schedule Patient Associations Notifications"),
	USER_PROFILE_UPDATE(12, "Profile Update");

	private Integer id;

	private String notification;

	public static final String values = "123";

	public static NotificationType getNotificationType(String notification) {
		return Arrays.stream(NotificationType.values())
		             .filter(type -> StringUtils.equalsIgnoreCase(type.getNotification(), notification))
		             .findFirst()
		             .orElse(null);
	}

	public static String generateValues() {
		return Arrays.stream(NotificationType.values())
		             .map(NotificationType::getNotification)
		             .collect(Collectors.joining(", "));
	}
}
