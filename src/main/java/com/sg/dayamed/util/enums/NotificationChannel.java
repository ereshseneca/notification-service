package com.sg.dayamed.util.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;

/**
 * Created By Gorantla, Eresh on 23/Nov/2019
 **/
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum NotificationChannel {
	EMAIL(1, "Email"),
	SMS(2, "SMS"),
	VIDEO(3, "Video"),
	PUSH(4, "Push");

	private Integer id;

	private String channel;

	public static NotificationChannel getNotificationChannel(String value) {
		return Arrays.stream(NotificationChannel.values())
		             .filter(channel -> StringUtils.equalsIgnoreCase(value, channel.getChannel()))
		             .findFirst()
		             .orElse(null);
	}
}
