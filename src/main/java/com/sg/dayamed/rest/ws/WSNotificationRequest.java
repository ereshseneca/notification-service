package com.sg.dayamed.rest.ws;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * Created By Gorantla, Eresh on 23/Nov/2019
 **/
@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "Model class for notifications request.")
public class WSNotificationRequest {

	@ApiModelProperty(name = "notifications",  value = "notifications", required = true)
	private List<@Valid WSNotification> notifications = new ArrayList<>();
}
