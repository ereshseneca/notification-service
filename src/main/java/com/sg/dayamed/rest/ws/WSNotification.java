package com.sg.dayamed.rest.ws;

import com.sg.dayamed.util.enums.NotificationChannel;
import com.sg.dayamed.util.enums.NotificationType;
import com.sg.dayamed.validators.EnumValueValidator;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.util.HashMap;
import java.util.Map;

/**
 * Created By Gorantla, Eresh on 23/Nov/2019
 **/
@Getter
@Setter
@NoArgsConstructor
@ApiModel(value = "notification", description = "Model Class for notification request")
public class WSNotification {

	@NotBlank(message = "{notification.type.required}")
	@EnumValueValidator(enumClass = NotificationType.class, message = "{notification.type.is.invalid}")
	@ApiModelProperty(value = "notificationType", notes = "Notification Type", name = "notificationType", required = true, position = 1)
	private String notificationType;

	@NotBlank(message = "{notification.channel.required}")
	@EnumValueValidator(enumClass = NotificationChannel.class, message = "{notification.channel.is.invalid}")
	@ApiModelProperty(value = "channel", notes = "Notification Channel", name = "channel", required = true, allowableValues = "Email, Sms, Push, Video",
	                  position = 2)
	private String channel;

	@NotBlank(message = "{notification.value.required}")
	@ApiModelProperty(value = "value", notes = "Notification value ie., email, sms, push token ...", name = "value", required = true, position = 0)
	private String value;

	@ApiModelProperty(value = "paramMap", notes = "Param Map Key, Value pairs", name = "paramMap", required = true, position = 5)
	private Map<String, Object> paramMap = new HashMap<>();

	@NotBlank(message = "{notification.language.key.required}")
	@ApiModelProperty(value = "languageKey", notes = "Language Key", name = "languageKey", required = true, allowableValues = "en, es", position = 3)
	private String languageKey;

	@ApiModelProperty(value = "deviceType", notes = "Notification deviceType iOS, android, web", name = "value", required = false, position = 4,
	                  allowableValues = "iOS, android, web")
	private String deviceType;
}
