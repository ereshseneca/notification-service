package com.sg.dayamed.rest.controller.mobile;

import io.swagger.annotations.Api;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created By Gorantla, Eresh on 25/Nov/2019
 **/
@RestController(value = NotificationController.RESOURCE_NAME + "_" + NotificationController.API_VERSION)
@RequestMapping("/api/" + NotificationController.API_VERSION)
@Api(value = "Operations related to Web Notifications")
public class NotificationController extends com.sg.dayamed.rest.controller.web.NotificationController {

	public static final String API_VERSION = "m";

	public static final String RESOURCE_NAME = "NotificationController";
}
