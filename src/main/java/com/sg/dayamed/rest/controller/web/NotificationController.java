package com.sg.dayamed.rest.controller.web;

import com.sg.dayamed.rest.commons.BaseRestApi;
import com.sg.dayamed.rest.commons.RestApiException;
import com.sg.dayamed.rest.commons.RestRequest;
import com.sg.dayamed.rest.commons.RestResponse;
import com.sg.dayamed.rest.ws.WSNotification;
import com.sg.dayamed.rest.ws.WSNotificationRequest;
import com.sg.dayamed.service.NotificationService;
import com.sg.dayamed.service.vo.NotificationVO;
import com.sg.dayamed.util.enums.NotificationChannel;
import com.sg.dayamed.util.enums.NotificationType;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jdk.nashorn.internal.objects.annotations.Getter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created By Gorantla, Eresh on 25/Nov/2019
 **/
@RestController(value = NotificationController.RESOURCE_NAME + "_" + NotificationController.API_VERSION)
@RequestMapping("/api/" + NotificationController.API_VERSION)
@Api(value = "Operations related to Web Notifications")
public class NotificationController extends BaseRestApi {

	public static final String API_VERSION = "w";

	public static final String RESOURCE_NAME = "NotificationController";

	@Autowired
	NotificationService notificationService;

	@PostMapping("/notifications")
	@ApiOperation(value = "Method returns calender notification events.", response = RestResponse.class, nickname = API_VERSION + "_" +
			RESOURCE_NAME + "_sendNotification")
	public ResponseEntity<RestResponse> sendNotification(@RequestBody @Valid WSNotificationRequest request) throws RestApiException {
		return inboundServiceCall(request, service -> {
			notificationService.sendNotifications(request.getNotifications()
			                                             .stream()
			                                             .map(NotificationVO::new)
			                                             .collect(Collectors.toList()));
			return ResponseEntity.ok(new RestResponse());
		});
	}

	@GetMapping("notifications")
	public ResponseEntity<RestResponse> sendNotification1() throws RestApiException {
		return inboundServiceCall(new RestRequest(), service -> {
			WSNotification notification = new WSNotification();
			notification.setChannel(NotificationChannel.EMAIL.getChannel());
			notification.setNotificationType(NotificationType.USER_PROFILE_UPDATE.getNotification());
			notification.setValue("eresh.zealous@gmail.com");
			notification.setLanguageKey("en");
			WSNotificationRequest notificationRequest = new WSNotificationRequest();
			notificationRequest.setNotifications(Stream.of(notification)
			                                           .collect(Collectors.toList()));
			notificationService.sendNotifications(notificationRequest.getNotifications()
			                                                         .stream()
			                                                         .map(NotificationVO::new)
			                                                         .collect(Collectors.toList()));
			return ResponseEntity.ok(new RestResponse());
		});
	}
}
