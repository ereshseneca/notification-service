package com.sg.dayamed.rest.commons;

import com.sg.dayamed.exceptions.ApplicationException;
import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.exceptions.ExceptionLogUtil;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

/**
 * Created By Gorantla, Eresh on 22/Nov/2019
 **/

public class BaseRestApi {

	@Autowired
	ObjectMapper objectMapper;

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	protected <I> ResponseEntity<RestResponse> inboundServiceCall(I request,
	                                                              IServiceMethod<I, ResponseEntity<RestResponse>> service)
			throws RestApiException {
		try {
			ResponseEntity<RestResponse> response = service.execute(request);
			return response;
		} catch (Exception e) {
			logError(e);
			throw processException(e);
		}
	}

	protected <I> ResponseEntity<Object> inboundServiceCallObjectResponse(I request,
	                                                                      IServiceMethod<I, ResponseEntity<Object>> service)
			throws RestApiException {
		try {
			ResponseEntity<Object> response = service.execute(request);
			return response;
		} catch (Exception e) {
			logError(e);
			throw processException(e);
		}
	}

	protected <I> ResponseEntity<Object> inboundServiceCallGeneric(I request,
	                                                               IServiceMethod<I, ResponseEntity<Object>> service)
			throws RestApiException {
		try {
			ResponseEntity<Object> response = service.execute(request);
			return response;
		} catch (Exception e) {
			logError(e);
			throw processException(e);
		}
	}

	private void logError(Exception e) {
		logger.error("Error processing Rest Request => ", e);
	}

	protected RestApiException processException(Exception e) {
		logger.debug("At: processException()");
		RestApiException restApiException = null;
		Throwable rootCause = ExceptionLogUtil.getRootCause(e);
		if (e instanceof DataValidationException) {
			restApiException = new RestApiException((DataValidationException) e);
		} else if (rootCause instanceof DataValidationException) {
			restApiException = new RestApiException((DataValidationException) rootCause);
		} else if (e instanceof ApplicationException) {
			restApiException = new RestApiException(e.getMessage(), RestApiException.createApplicationExceptionFault(
					(ApplicationException) e));
		}

		if (restApiException == null) {
			restApiException = new RestApiException(e.getMessage(), RestApiException.createUnexpectedFault(e));
		}
		// restApiException.setMethod(method);
		return restApiException;
	}

}
