package com.sg.dayamed.rest.commons;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * Created By Gorantla, Eresh on 23/Nov/2019
 **/
@Getter
@Setter
public class RestResponse {

	@ApiModelProperty(required = true, notes = "Result like Successful, DataValidation Error are shown")
	private String result = IResponseCodes.SUCCESSFUL;
}

