package com.sg.dayamed.redis;

import com.sg.dayamed.component.NotificationTemplateUtils;
import com.sg.dayamed.util.enums.NotificationChannel;
import com.sg.dayamed.util.enums.NotificationType;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;

/**
 * Created By Gorantla, Eresh on 25/Nov/2019
 **/
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum NotificationKeysEnum {

	RESET_PASSWORD_EMAIL_TEMPLATE_EN(1, NotificationTemplateUtils.DEFAULT_LANGUAGE, NotificationType.RESET_PASSWORD.getNotification(),
	                                 "dayamed.notification.reset.password.email.template.en", NotificationChannel.EMAIL.getChannel()),
	RESET_PASSWORD_EMAIL_TEMPLATE_ES(2, NotificationTemplateUtils.SPANISH_LANGUAGE, NotificationType.RESET_PASSWORD.getNotification(),
	                                 "dayamed.notification.reset.password.email.template.es", NotificationChannel.EMAIL.getChannel()),
	RESET_PASSWORD_EMAIL_SUBJECT_EN(3, NotificationTemplateUtils.DEFAULT_LANGUAGE, NotificationType.RESET_PASSWORD.getNotification(),
	                                "dayamed.notification.reset.password.email.subject.en", NotificationChannel.EMAIL.getChannel()),
	RESET_PASSWORD_SMS_TEMPLATE_EN(3, NotificationTemplateUtils.DEFAULT_LANGUAGE, NotificationType.RESET_PASSWORD.getNotification(),
	                               "dayamed.notification.reset.password.sms.template.en", NotificationChannel.SMS.getChannel()),

	UPDATE_PROFILE_EMAIL_TEMPLATE_EN(4, NotificationTemplateUtils.DEFAULT_LANGUAGE, NotificationType.USER_PROFILE_UPDATE.getNotification(),
	                                 "dayamed.notification.update.profile.email.template.en", NotificationChannel.EMAIL.getChannel()),
	UPDATE_PROFILE_EMAIL_TEMPLATE_ES(5, NotificationTemplateUtils.SPANISH_LANGUAGE, NotificationType.USER_PROFILE_UPDATE.getNotification(),
	                                 "dayamed.notification.update.profile.email.template.es", NotificationChannel.EMAIL.getChannel()),
	UPDATE_PROFILE_EMAIL_SUBJECT_EN(6, NotificationTemplateUtils.SPANISH_LANGUAGE, NotificationType.USER_PROFILE_UPDATE.getNotification(),
	                                "dayamed.notification.update.profile.email.subject.en", NotificationChannel.EMAIL.getChannel()),
	UPDATE_PROFILE_EMAIL_SUBJECT_ES(7, NotificationTemplateUtils.SPANISH_LANGUAGE, NotificationType.USER_PROFILE_UPDATE.getNotification(),
	                                "dayamed.notification.update.profile.email.subject.en", NotificationChannel.EMAIL.getChannel()),
	UPDATE_PROFILE_SMS_TEMPLATE_EN(4, NotificationTemplateUtils.DEFAULT_LANGUAGE, NotificationType.USER_PROFILE_UPDATE.getNotification(),
	                               "dayamed.notification.update.profile.sms.template.en", NotificationChannel.SMS.getChannel()),
	UPDATE_PROFILE_SMS_TEMPLATE_ES(5, NotificationTemplateUtils.SPANISH_LANGUAGE, NotificationType.USER_PROFILE_UPDATE.getNotification(),
	                               "dayamed.notification.update.profile.sms.template.en", NotificationChannel.SMS.getChannel());

	private Integer id;

	private String langKey;

	private String notificationType;

	private String key;

	private String channel;

	public static NotificationKeysEnum getNotificationValues(String notificationType, String channel, String lang) {
		return Arrays.stream(NotificationKeysEnum.values())
		             .filter(data -> StringUtils.equalsIgnoreCase(channel, data.getChannel()) &&
				             StringUtils.equalsIgnoreCase(notificationType, data.getNotificationType()) &&
				             StringUtils.equalsIgnoreCase(lang, data.getLangKey()))
		             .findFirst()
		             .orElse(null);
	}

}
