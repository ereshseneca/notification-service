package com.sg.dayamed.redis;

import com.sg.dayamed.util.NotificationServiceConstants;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * Created By Gorantla, Eresh on 23/Nov/2019
 **/
@Service
public class RedisServiceImpl implements RedisService {

	@Autowired
	RedisTemplate<String, Object> redisTemplate;

	@Override
	public Object getDataInRedisByKey(String key) {
		ValueOperations<String, Object> value = redisTemplate.opsForValue();
		return value.get(key);
	}

	@Override
	public Object getDataInRedisByKeyOrFail(String key) {
		final Object dataInRedisByKey = getDataInRedisByKey(key);
		if (dataInRedisByKey == null) {
			throw new RuntimeException(String.format("The key %s does not exist in Redis.", key));
		}
		return dataInRedisByKey;
	}

	@Override
	public String configParamsUpdateInRedis(String keyObj, Object valueObject) {
		ValueOperations<String, Object> value = redisTemplate.opsForValue();
		value.set(keyObj, valueObject);
		return NotificationServiceConstants.SUCCESS_STATUS;
	}

	@Override
	public String saveObjectWithExpire(String keyObject, Object valueObj, int expireTime) {
		ValueOperations<String, Object> value = redisTemplate.opsForValue();
		value.set(keyObject, valueObj, expireTime, TimeUnit.MINUTES);
		return NotificationServiceConstants.SUCCESS_STATUS;
	}
}
