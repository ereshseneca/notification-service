package com.sg.dayamed.redis;

/**
 * Created By Gorantla, Eresh on 23/Nov/2019
 **/

public interface RedisService {

	Object getDataInRedisByKey(String key);

	Object getDataInRedisByKeyOrFail(String key);

	String configParamsUpdateInRedis(String keyObj, Object valueObject);

	String saveObjectWithExpire(String keyObject, Object valueObj, int expireTime);
}
