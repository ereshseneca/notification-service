package com.sg.dayamed.redis;

/**
 * Created By Gorantla, Eresh on 23/Nov/2019
 **/

public interface RedisConstants {

	String PRODUCT_NAME = "dayamed.notification.product.name";

	String UPDATE_PROFILE_EMAIL_TEMPLATE_EN = "dayamed.notification.update.profile.email.template.en";

	String UPDATE_PROFILE_EMAIL_TEMPLATE_ES = "dayamed.notification.update.profile.email.template.es";

	String UPDATE_PROFILE_SMS_TEMPLATE_EN = "dayamed.notification.update.profile.sms.template.en";

	String UPDATE_PROFILE_SMS_TEMPLATE_ES = "dayamed.notification.update.profile.sms.template.es";

	String UPDATE_PROFILE_EMAIL_SUBJECT_EN = "dayamed.notification.update.profile.email.subject.en";

	String UPDATE_PROFILE_EMAIL_SUBJECT_ES = "dayamed.notification.update.profile.email.subject.es";

	String TWILIO_ACCOUNT_SID = "dayamed.notification.twilio.account.sid";

	String TWILIO_AUTH_TOKEN = "dayamed.notification.twilio.auth.token";

	String TWILIO_FROM_CONTACT = "dayamed.notification.twilio.from.contact";

	String SMS_SERVICE_ENABLE = "dayamed.notification.sms.service.enable";

	String RESET_PASSWORD_EMAIL_TEMPLATE_EN = "dayamed.notification.reset.password.email.template.en";

	String RESET_PASSWORD_SMS_TEMPLATE_EN = "dayamed.notification.reset.password.sms.template.en";

	String RESET_PASSWORD_EMAIL_SUBJECT_EN = "dayamed.notification.reset.password.email.subject.en";

	String PRESCRIPTION_UPDATE_EMAIL_TEMPLATE_EN = "dayamed.notification.prescription.modify.email.template.en";

	String PRESCRIPTION_UPDATE_EMAIL_TEMPLATE_ES = "dayamed.notification.prescription.modify.email.template.es";

	String PRESCRIPTION_UPDATE_SMS_TEMPLATE_EN = "dayamed.notification.prescription.modify.sms.template.en";

	String PRESCRIPTION_UPDATE_SMS_TEMPLATE_ES = "dayamed.notification.prescription.modify.sms.template.es";

	String PRESCRIPTION_UPDATE_EMAIL_SUBJECT_EN = "dayamed.notification.prescription.modify.email.subject.en";

	String PRESCRIPTION_UPDATE_EMAIL_SUBJECT_ES = "dayamed.notification.prescription.modify.email.subject.es";
}
