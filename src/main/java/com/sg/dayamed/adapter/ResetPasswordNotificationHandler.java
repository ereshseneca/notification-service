package com.sg.dayamed.adapter;

import com.sg.dayamed.component.vo.EmailMessageVO;
import com.sg.dayamed.component.vo.MessageModelVO;
import com.sg.dayamed.exceptions.ExceptionLogUtil;
import com.sg.dayamed.service.vo.NotificationVO;
import com.sg.dayamed.util.enums.NotificationType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created By Gorantla, Eresh on 25/Nov/2019
 **/
@Service
public class ResetPasswordNotificationHandler extends BaseNotificationHandler {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	ResetPasswordNotificationHandler self;

	@Override
	public BaseNotificationHandler getNotificationTypeHandler() {
		return self;
	}

	@Override
	public String[] getNotificationTypes() {
		return new String[]{NotificationType.RESET_PASSWORD.getNotification()};
	}

	@Override
	public EmailMessageVO processEmail(NotificationVO notificationVO) {
		try {
			String content = notificationTemplateUtils.getResetPasswordEmailContent(notificationVO.getLangKey());
			content = messageFormatter.formatNotificationContent(content, notificationVO.getParamMap());
			EmailMessageVO messageVO =
					new EmailMessageVO(notificationVO, content, notificationTemplateUtils.getResetPasswordEmailSubject(notificationVO.getLangKey()));
			return messageVO;
		} catch (Exception e) {
			ExceptionLogUtil.log(e, "Error Processing -> " + notificationVO.getNotificationType() + " For Channel -> " + notificationVO.getChannel(), logger);
		}
		return null;
	}

	@Override
	public MessageModelVO processSMS(NotificationVO notificationVO) {
		try {
			String content = notificationTemplateUtils.getResetPasswordSmsContent(notificationVO.getLangKey());
			content = messageFormatter.formatNotificationContent(content, notificationVO.getParamMap());
			MessageModelVO modelVO = new MessageModelVO(notificationVO, content);
			return modelVO;
		} catch (Exception e) {
			ExceptionLogUtil.log(e, "Error Processing -> " + notificationVO.getNotificationType() + " For Channel -> " + notificationVO.getChannel(), logger);
		}
		return null;
	}
}
