package com.sg.dayamed.adapter;

import com.sg.dayamed.component.NotificationTemplateUtils;
import com.sg.dayamed.exceptions.ExceptionLogUtil;
import com.sg.dayamed.queues.EmailSender;
import com.sg.dayamed.queues.SmsSender;
import com.sg.dayamed.service.NotificationResultService;
import com.sg.dayamed.component.vo.EmailMessageVO;
import com.sg.dayamed.component.vo.MessageModelVO;
import com.sg.dayamed.service.vo.NotificationResultVO;
import com.sg.dayamed.service.vo.NotificationVO;
import com.sg.dayamed.util.NotificationServiceConstants;
import com.sg.dayamed.util.enums.NotificationChannel;
import com.sg.dayamed.util.enums.NotificationType;
import com.sg.dayamed.velocity.VelocityMessageFormatter;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.annotation.PostConstruct;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;

/**
 * Created By Gorantla, Eresh on 23/Nov/2019
 **/

public abstract class BaseNotificationHandler {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	NotificationsAdapter adapter;

	@Autowired
	VelocityMessageFormatter messageFormatter;

	@Autowired
	EmailSender emailSender;

	@Autowired
	SmsSender smsSender;

	@Autowired
	NotificationTemplateUtils notificationTemplateUtils;

	@PostConstruct
	public void init() {
		adapter.registerHandler(getNotificationTypeHandler(), getNotificationTypes());
	}

	protected abstract BaseNotificationHandler getNotificationTypeHandler();

	protected abstract String[] getNotificationTypes();

	protected void sendEmail(EmailMessageVO messageVO, NotificationVO notificationVO) {
		try {
			emailSender.sendEmail(messageVO);
		} catch (Exception e) {
			ExceptionLogUtil.log(e, "Error Processing in sending to email Queue", logger);
		}
	}

	protected void sendSMS(MessageModelVO modelVO, NotificationVO notificationVO) {
		try {
			smsSender.sendSMS(modelVO);
		} catch (Exception e) {
			ExceptionLogUtil.log(e, "Error Processing in sending to sms Queue", logger);
		}
	}

	protected abstract EmailMessageVO processEmail(NotificationVO notificationVO);

	protected abstract MessageModelVO processSMS(NotificationVO notificationVO);

	protected void sendPushNotification(NotificationVO notificationVO) {

	}
}
