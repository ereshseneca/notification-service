package com.sg.dayamed.adapter;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Created By Gorantla, Eresh on 23/Nov/2019
 **/
@Component
public class NotificationsAdapter {

	private Map<String, BaseNotificationHandler> handlers = new HashMap<>();

	public BaseNotificationHandler getHandler(String userType) {
		return handlers.get(userType);
	}

	public void registerHandler(BaseNotificationHandler handler, String... notificationTypes) {
		if (handler == null) {
			throw new IllegalArgumentException("Handler cannot be null");
		}

		if (ArrayUtils.isEmpty(notificationTypes)) {
			throw new IllegalArgumentException("import types cannot be null or empty");
		}

		for (String importType : notificationTypes) {
			handlers.put(importType, handler);
		}
	}
}
