package com.sg.dayamed.adapter;

import com.sg.dayamed.redis.RedisConstants;
import com.sg.dayamed.redis.RedisService;
import com.sg.dayamed.service.NotificationResultService;
import com.sg.dayamed.service.vo.NotificationResultVO;
import com.sg.dayamed.service.vo.NotificationVO;
import com.sg.dayamed.util.NotificationServiceConstants;
import com.sg.dayamed.util.enums.NotificationChannel;
import com.sg.dayamed.util.enums.NotificationType;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;

/**
 * Created By Gorantla, Eresh on 23/Nov/2019
 **/
@Component
public class NotificationProcessor {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	NotificationsAdapter adapter;

	@Autowired
	@Qualifier("notificationResult")
	ExecutorService notificationResultExecutorService;

	@Autowired
	NotificationResultService notificationResultService;

	@Autowired
	RedisService redisService;

	public void sendNotification(List<NotificationVO> notificationVOS) {
		notificationVOS.stream()
		               .forEach(notificationVO -> {
			               try {
				               NotificationType notificationType = NotificationType.getNotificationType(notificationVO.getNotificationType());
				               if (notificationType != null) {
					               BaseNotificationHandler handler = adapter.getHandler(notificationType.getNotification());
					               if (handler != null) {
						               if (NotificationChannel.EMAIL.getChannel()
						                                            .equalsIgnoreCase(notificationVO.getChannel())) {
							               handler.sendEmail(handler.processEmail(notificationVO), notificationVO);
						               }
						               if (NotificationChannel.SMS.getChannel()
						                                          .equalsIgnoreCase(notificationVO.getChannel()) &&
								               Boolean.parseBoolean((String) redisService.getDataInRedisByKeyOrFail(RedisConstants.SMS_SERVICE_ENABLE))) {
							               handler.sendSMS(handler.processSMS(notificationVO), notificationVO);
						               }
					               }
				               }
			               } catch (Exception e) {
				               logger.error("Error occurred while sending notification", e);
				               CompletableFuture.runAsync(() -> createNotificationResult(notificationVO, e), notificationResultExecutorService);
			               }
		               });
	}

	public void createNotificationResult(NotificationVO notificationVO, Exception exc) {
		try {
			NotificationResultVO resultVO = new NotificationResultVO();
			resultVO.setChannel(notificationVO.getChannel());
			resultVO.setResult(NotificationServiceConstants.FAILURE_STATUS);
			resultVO.setStackTrace(ExceptionUtils.getStackTrace(exc));
			resultVO.setType(notificationVO.getNotificationType());
			resultVO.setValue(notificationVO.getValue());
			notificationResultService.saveNotificationResult(resultVO);
		} catch (Exception e) {
			logger.error("Error occurred while saving notification result in Processor", e);
		}
	}
}
