package com.sg.dayamed.velocity;

import org.apache.commons.lang3.StringUtils;
import org.apache.velocity.VelocityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.StringWriter;
import java.util.Map;
import java.util.UUID;

/**
 * Created By Gorantla, Eresh on 23/Nov/2019
 **/
@Component
public class VelocityMessageFormatter {

	@Autowired
	VelocityEngine velocityEngine;

	public String formatNotificationContent(String content, Map<String, Object> params) {
		if (StringUtils.isBlank(content)) {
			throw new IllegalArgumentException("email content must not be null.");
		}
		VelocityContext context = new VelocityContext();
		context = updateParameters(params, context);
		StringWriter writer = new StringWriter();
		velocityEngine.evaluate(context, writer, UUID.randomUUID()
		                                             .toString(), content);
		return writer.toString();
	}

	private VelocityContext updateParameters(Map<String, Object> params, VelocityContext context) {
		if (params != null && !params.isEmpty()) {
			params.forEach((key, value) -> {
				context.put(key, value);
			});
		}
		return context;
	}
}
