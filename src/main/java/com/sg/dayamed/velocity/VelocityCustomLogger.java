package com.sg.dayamed.velocity;

import org.apache.velocity.runtime.RuntimeServices;
import org.apache.velocity.runtime.log.LogChute;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created By Gorantla, Eresh on 30/Oct/2019
 **/

public class VelocityCustomLogger implements LogChute {

	private Logger logger = LoggerFactory.getLogger("velocity");

	public Logger getLogger() {
		return logger;
	}

	@Override
	public void init(RuntimeServices services) throws Exception {
		logger = LoggerFactory.getLogger("velocity");
	}

	@Override
	public boolean isLevelEnabled(int level) {
		switch (level) {
			case LogChute.DEBUG_ID:
				return logger.isDebugEnabled();
			case LogChute.TRACE_ID:
				return logger.isTraceEnabled();
			case LogChute.WARN_ID:
				return logger.isWarnEnabled();
			case LogChute.ERROR_ID:
				return logger.isErrorEnabled();
			default:
			case LogChute.INFO_ID:
				return logger.isInfoEnabled();
		}
	}

	@Override
	public void log(int level, String message) {
		log(level, message, null);
	}

	@Override
	public void log(int level, String message, Throwable t) {
		switch (level) {
			case LogChute.DEBUG_ID:
				logger.debug(message, t);
				break;
			case LogChute.TRACE_ID:
				logger.trace(message, t);
				break;
			case LogChute.WARN_ID:
				logger.warn(message, t);
				break;
			case LogChute.ERROR_ID:
				logger.error(message, t);
				break;
			default:
			case LogChute.INFO_ID:
				logger.info(message, t);
				break;
		}

	}
}
