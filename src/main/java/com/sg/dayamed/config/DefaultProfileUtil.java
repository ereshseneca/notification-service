package com.sg.dayamed.config;

import org.springframework.boot.SpringApplication;

import java.util.HashMap;
import java.util.Map;

/**
 * Created By Gorantla, Eresh on 22/Nov/2019
 **/

public class DefaultProfileUtil {

	private static final String SPRING_PROFILE_DEFAULT = "spring.profiles.default";

	/**
	 * Set a default to use when no profile is configured.
	 *
	 * @param app the Spring application
	 */
	public static void addDefaultProfile(SpringApplication app) {
		Map<String, Object> defProperties = new HashMap<>();
		defProperties.put(SPRING_PROFILE_DEFAULT, "local");
		app.setDefaultProperties(defProperties);
	}
}
