package com.sg.dayamed.config;

import com.sg.dayamed.rest.commons.RestFault;

import com.fasterxml.classmate.TypeResolver;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created By Gorantla, Eresh on 22/Nov/2019
 **/
@Configuration
@EnableSwagger2
@Profile({"local", "vm", "test"})
public class SwaggerConfiguration {

	private List<ResponseMessage> responseMessages =
			Stream.of(new ResponseMessageBuilder().code(400)
			                                      .responseModel(new ModelRef("RestFault"))
			                                      .message("Bad request")
			                                      .build(),
			          new ResponseMessageBuilder().code(404)
			                                      .responseModel(new ModelRef("RestFault"))
			                                      .message("Not Found")
			                                      .build(),
			          new ResponseMessageBuilder().code(405)
			                                      .responseModel(new ModelRef("RestFault"))
			                                      .message("Method Not Allowed")
			                                      .build(),
			          new ResponseMessageBuilder().code(409)
			                                      .responseModel(new ModelRef("RestFault"))
			                                      .message("Conflict")
			                                      .build(),
			          new ResponseMessageBuilder().code(401)
			                                      .responseModel(new ModelRef("RestFault"))
			                                      .message("Unauthorized")
			                                      .build(),
			          new ResponseMessageBuilder().code(403)
			                                      .responseModel(new ModelRef("RestFault"))
			                                      .message("Forbidden")
			                                      .build(),
			          new ResponseMessageBuilder().code(500)
			                                      .responseModel(new ModelRef("RestFault"))
			                                      .message("Internal Server Error")
			                                      .build())

			      .collect(Collectors.toList());

	@Bean
	public Docket webSecuredApi() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.sg.dayamed.rest.controller.web"))
				.paths(PathSelectors.regex("/api/w.*"))
				.build()
				.apiInfo(apiEndPointsInfo())
				.forCodeGeneration(true)
				.groupName("Web Secured Apis")
				.useDefaultResponseMessages(false)
				.globalOperationParameters(Arrays.asList(new ParameterBuilder().name("Authorization")
				                                                               .description("Description of header")
				                                                               .modelRef(new ModelRef("string"))
				                                                               .parameterType("header")
				                                                               .required(true)
				                                                               .build()));
	}

	@Bean
	public Docket mobileSecuredApi() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.sg.dayamed.rest.controller.mobile"))
				.paths(PathSelectors.regex("/api/m.*"))
				.build()
				.apiInfo(apiEndPointsInfo())
				.forCodeGeneration(true)
				.groupName("Mobile Secured Apis")
				.useDefaultResponseMessages(false)
				.additionalModels(new TypeResolver().resolve(RestFault.class))
				.globalResponseMessage(RequestMethod.POST, responseMessages)
				.globalResponseMessage(RequestMethod.GET, responseMessages)
				.globalResponseMessage(RequestMethod.PUT, responseMessages)
				.globalResponseMessage(RequestMethod.DELETE, responseMessages)
				.globalResponseMessage(RequestMethod.PATCH, responseMessages)
				.globalOperationParameters(Arrays.asList(new ParameterBuilder().name("Authorization")
				                                                               .description("Description of header")
				                                                               .modelRef(new ModelRef("string"))
				                                                               .parameterType("header")
				                                                               .required(true)
				                                                               .build()));
	}

	@Bean
	public Docket webUnsecuredApi() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.sg.dayamed.rest.controller.web"))
				.paths(PathSelectors.regex("/w.*"))
				.build()
				.apiInfo(apiEndPointsInfo())
				.forCodeGeneration(true)
				.additionalModels(new TypeResolver().resolve(RestFault.class))
				.groupName("Web Unsecured Apis")
				.useDefaultResponseMessages(false)
				.globalResponseMessage(RequestMethod.POST, responseMessages)
				.globalResponseMessage(RequestMethod.GET, responseMessages)
				.globalResponseMessage(RequestMethod.PUT, responseMessages)
				.globalResponseMessage(RequestMethod.DELETE, responseMessages)
				.globalResponseMessage(RequestMethod.PATCH, responseMessages);
	}

	@Bean
	public Docket mobileUnsecuredApi() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.sg.dayamed.rest.controller.mobile"))
				.paths(PathSelectors.regex("/m.*"))
				.build()
				.apiInfo(apiEndPointsInfo())
				.forCodeGeneration(true)
				.additionalModels(new TypeResolver().resolve(RestFault.class))
				.groupName("Mobile Unsecured Apis")
				.useDefaultResponseMessages(false)
				.globalResponseMessage(RequestMethod.POST, responseMessages)
				.globalResponseMessage(RequestMethod.GET, responseMessages)
				.globalResponseMessage(RequestMethod.PUT, responseMessages)
				.globalResponseMessage(RequestMethod.DELETE, responseMessages)
				.globalResponseMessage(RequestMethod.PATCH, responseMessages);
	}

	private ApiInfo apiEndPointsInfo() {
		return new ApiInfoBuilder().title("Notification Application")
		                           .description("Notification REST API")
		                           .contact(new Contact("Dayamed Server Team", "#", "dayamed-dev@senecaglobal.com"))
		                           .license("Apache 2.0")
		                           .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html")
		                           .version("1.0.0")
		                           .build();
	}
}
