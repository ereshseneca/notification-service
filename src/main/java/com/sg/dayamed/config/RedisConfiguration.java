package com.sg.dayamed.config;

import com.sg.dayamed.properties.RedisProperties;

import redis.clients.jedis.JedisPoolConfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisPassword;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisClientConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import java.net.UnknownHostException;
import java.time.Duration;

/**
 * Created By Gorantla, Eresh on 23/Nov/2019
 **/
@Configuration
public class RedisConfiguration extends CachingConfigurerSupport {

	@Autowired
	RedisProperties redisProperties;

	@Bean
	public RedisConnectionFactory redisConnectionFactory() {
		JedisPoolConfig poolConfig = new JedisPoolConfig();
		poolConfig.setMaxTotal(redisProperties.getMaxTotal());
		poolConfig.setMinIdle(redisProperties.getMinIdle());
		poolConfig.setMaxIdle(redisProperties.getMaxIdle());
		poolConfig.setMaxWaitMillis(redisProperties.getMaxWaitMillis());
		poolConfig.setTestOnBorrow(redisProperties.getTestOnBorrow());
		poolConfig.setNumTestsPerEvictionRun(redisProperties.getNumTestsPerEvictionRun());
		poolConfig.setJmxEnabled(true);
		poolConfig.setJmxNamePrefix(redisProperties.getJmsPrefix());
		RedisStandaloneConfiguration redisStandaloneConfiguration = new RedisStandaloneConfiguration();
		redisStandaloneConfiguration.setHostName(redisProperties.getHost());
		redisStandaloneConfiguration.setPort(redisProperties.getPort());
		redisStandaloneConfiguration.setPassword(RedisPassword.of(redisProperties.getPassword()));

		JedisClientConfiguration.JedisClientConfigurationBuilder clientConfigurationBuilder = JedisClientConfiguration.builder();
		clientConfigurationBuilder.connectTimeout(Duration.ofSeconds(60));// 60s connection timeout
		clientConfigurationBuilder.usePooling()
		                          .poolConfig(poolConfig)
		                          .build();

		JedisConnectionFactory connectionFactory = new JedisConnectionFactory(redisStandaloneConfiguration,
		                                                                      clientConfigurationBuilder.build());

		return connectionFactory;
	}

	@Bean(name = "redisUserTemplate")
	public RedisTemplate<String, Object> redisTemplate() {
		RedisTemplate<String, Object> template = new RedisTemplate<>();
		template.setConnectionFactory(redisConnectionFactory());
		template.setDefaultSerializer(new GenericJackson2JsonRedisSerializer());
		template.setKeySerializer(new StringRedisSerializer());
		template.setHashKeySerializer(new GenericJackson2JsonRedisSerializer());
		template.setValueSerializer(new GenericJackson2JsonRedisSerializer());
		return template;
	}

	@Bean
	public RedisCacheConfiguration cacheConfiguration() {
		RedisCacheConfiguration cacheConfig = RedisCacheConfiguration.defaultCacheConfig()
		                                                             .entryTtl(Duration.ofSeconds(600))
		                                                             .disableCachingNullValues();
		return cacheConfig;
	}

	@Bean
	public RedisCacheManager cacheManager() {
		RedisCacheManager rcm = RedisCacheManager.builder(redisConnectionFactory())
		                                         .cacheDefaults(cacheConfiguration())
		                                         .transactionAware()
		                                         .build();
		return rcm;
	}

}
