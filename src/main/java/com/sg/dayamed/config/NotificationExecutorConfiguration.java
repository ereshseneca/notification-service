package com.sg.dayamed.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created By Gorantla, Eresh on 23/Nov/2019
 **/
@Configuration
public class NotificationExecutorConfiguration {

	@Bean("notificationResult")
	public ExecutorService notificationResultExecutorService() {
		ExecutorService executorService = new ThreadPoolExecutor(10, 20, 5, TimeUnit.MINUTES, new ArrayBlockingQueue<>(100));
		return executorService;
	}
}
