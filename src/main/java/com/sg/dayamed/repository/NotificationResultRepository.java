package com.sg.dayamed.repository;

import com.sg.dayamed.entity.NotificationResult;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created By Gorantla, Eresh on 23/Nov/2019
 **/
@Repository
public interface NotificationResultRepository extends JpaRepository<NotificationResult, String> {

}
