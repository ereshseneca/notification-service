package com.sg.dayamed.validators;

import com.sg.dayamed.util.enums.NotificationChannel;
import com.sg.dayamed.util.enums.NotificationType;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;

/**
 * Created By Gorantla, Eresh on 25/Nov/2019
 **/

public class EnumValueValidatorImpl implements ConstraintValidator<EnumValueValidator, String> {

	private EnumValueValidator annotation;

	@Override
	public void initialize(EnumValueValidator annotation) {
		this.annotation = annotation;
	}

	@Override
	public boolean isValid(String valueForValidation, ConstraintValidatorContext constraintValidatorContext) {
		if (valueForValidation == null) {
			return false;
		}
		Object[] enumValues = this.annotation.enumClass()
		                                     .getEnumConstants();
		if (ArrayUtils.isNotEmpty(enumValues)) {
			if (enumValues[0] instanceof NotificationType) {
				return Arrays.stream(enumValues)
				             .map(value -> (NotificationType) value)
				             .filter(value -> StringUtils.equalsIgnoreCase(value.getNotification(), valueForValidation))
				             .findFirst()
				             .orElse(null) != null;
			}
			if (enumValues[0] instanceof NotificationChannel) {
				return Arrays.stream(enumValues)
				             .map(value -> (NotificationChannel) value)
				             .filter(value -> StringUtils.equalsIgnoreCase(value.getChannel(), valueForValidation))
				             .findFirst()
				             .orElse(null) != null;
			}
		}
		return false;
	}
}
