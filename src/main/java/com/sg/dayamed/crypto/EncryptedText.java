package com.sg.dayamed.crypto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created By Gorantla, Eresh on 23/Nov/2019
 **/
@Getter
@Setter
public class EncryptedText {

	private String cipherText;

	private String keyName;

	private String iv;

	public EncryptedText() {
	}

	public EncryptedText(String cipherText) {
		this.cipherText = cipherText;
		this.keyName = null;
		this.iv = null;
	}

}
