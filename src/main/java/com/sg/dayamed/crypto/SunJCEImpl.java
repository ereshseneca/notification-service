package com.sg.dayamed.crypto;

import org.apache.commons.codec.binary.Hex;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.spec.KeySpec;
import java.util.Base64;

/**
 * Created By Gorantla, Eresh on 23/Nov/2019
 **/

public class SunJCEImpl extends ProviderBaseClass implements ProviderInterface {
	private final String KEY = "JTvkKblzXeUmR2SFHn6LziebFidND91X9o7nTGU2JfJzZjmrGlfSNUlQgBYBlQfIFBZCTUJFE4YjRcgmSx9ogmFbMQbVDzUsflHJuUXB1qyQJEP18fStV8OKbfXZssqg";
	private final String SALT_VALUE = "3FF2EC019C627B945225DEBAD71A01B6985FE84C95A70EB132882F88C0A59A55";
	private final String INIT_VECTOR = "12345678901234567890123456789012";
	private final int pswdIterations = 10;
	private final int keySize = 128;


	public void init() {

	}

	@Override
	public ClearText decrypt(EncryptedText encryptedText) {
		return decryptAES(encryptedText.getCipherText());
	}

	@Override
	public EncryptedText encrypt(ClearText clearText) {
		return encryptAES(clearText.getText());
	}

	private ClearText decryptAES(String encryptedText) {
		try {
			Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
			SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
			KeySpec spec = new PBEKeySpec(KEY.toCharArray(), Hex.decodeHex(SALT_VALUE.toCharArray()), pswdIterations, keySize);
			SecretKey key = new SecretKeySpec(factory.generateSecret(spec).getEncoded(), "AES");
			SecretKeySpec keySpec = new SecretKeySpec(key.getEncoded(), "AES");
			GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(12 * 10, INIT_VECTOR.getBytes());
			cipher.init(Cipher.DECRYPT_MODE, keySpec, gcmParameterSpec);
			byte[] encryptedTextBytes = cipher.doFinal(Base64.getDecoder().decode(encryptedText));
			String decryptedText = new String(encryptedTextBytes);
			ClearText clearText = new ClearText();
			clearText.setText(decryptedText);
			clearText.setIv(ProviderInterface.CRYPTO_PROVIDER_SUN);
			return clearText;
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	private EncryptedText encryptAES(String clearText) {
		try {
			Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
			SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
			KeySpec spec = new PBEKeySpec(KEY.toCharArray(), Hex.decodeHex(SALT_VALUE.toCharArray()), pswdIterations, keySize);
			SecretKey key = new SecretKeySpec(factory.generateSecret(spec).getEncoded(), "AES");
			SecretKeySpec keySpec = new SecretKeySpec(key.getEncoded(), "AES");
			GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(12 * 10, INIT_VECTOR.getBytes());
			cipher.init(Cipher.ENCRYPT_MODE, keySpec, gcmParameterSpec);
			byte[] encryptedTextBytes = cipher.doFinal(clearText.getBytes());
			String encrypted = Base64.getEncoder().encodeToString(encryptedTextBytes);
			EncryptedText encryptedText = new EncryptedText();
			encryptedText.setCipherText(encrypted);
			encryptedText.setIv(ProviderInterface.CRYPTO_PROVIDER_SUN);
			return encryptedText;
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}
}
