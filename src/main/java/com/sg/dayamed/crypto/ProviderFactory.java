package com.sg.dayamed.crypto;

import java.security.Provider;
import java.security.Security;

/**
 * Created By Gorantla, Eresh on 23/Nov/2019
 **/

public class ProviderFactory {

	private ProviderFactory() {
	}


	public static ProviderBaseClass create(String provider) throws Exception {
		String className = null;

		try {
			// create instance
			className = "com.sg.dayamed.crypto." + provider + "Impl";
			ProviderBaseClass providerBaseClass = (ProviderBaseClass) Class.forName(className).newInstance();

			// initialize instance
			providerBaseClass.init();

			if (isProviderInstalled(provider)) {
				return providerBaseClass;
			} else {
				throw new RuntimeException("Provider not installed in JRE: " + provider);
			}
		} catch (Throwable e) {
			throw new Exception("Can't instantiate: " +e);
		}
	}

	private static boolean isProviderInstalled(String provider) {

		if (ProviderInterface.CRYPTO_PROVIDER_NONE.equals(provider)) {
			return true;
		}

		Provider[] providers = Security.getProviders();
		if (providers != null) {
			for (int i = 0; i < providers.length; i++) {
				if (providers[i].getName().equals(provider)) {
					return true;
				}
			}
		}
		return false;
	}
}
