package com.sg.dayamed.crypto;

/**
 * Created By Gorantla, Eresh on 23/Nov/2019
 **/

public class SecurityUtil {

	private SecurityUtil() {
	}

	/**
	 * Encrypts the given String and returns the encrypted text. The client is
	 * responsible for any exceptions.
	 */
	public static String encryptText(String clearText) throws Exception {
		ProviderBaseClass provider = ProviderFactory.create(ProviderInterface.CRYPTO_PROVIDER_SUN);
		return provider.encrypt(new ClearText(clearText)).getCipherText();
	}

	/**
	 * Decrypts the given String and returns the unencrypted text The client is
	 * responsible for any exceptions. If an exception is thrown that usually
	 * means the text was not encrypted.
	 * @throws Exception
	 */
	public static String decryptText(String encryptedText) throws Exception {
		ProviderBaseClass provider = ProviderFactory.create(ProviderInterface.CRYPTO_PROVIDER_SUN);
		return provider.decrypt(new EncryptedText(encryptedText)).getText();
	}
}
