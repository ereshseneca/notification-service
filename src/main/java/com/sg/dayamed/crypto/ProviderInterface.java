package com.sg.dayamed.crypto;

/**
 * Created By Gorantla, Eresh on 23/Nov/2019
 **/

public interface ProviderInterface {
	public static final String CRYPTO_PROVIDER_NONE = "None";
	public static final String CRYPTO_PROVIDER_SUN = "SunJCE";

	// required methods
	public abstract ClearText decrypt(EncryptedText encryptedText);

	public abstract EncryptedText encrypt(ClearText clearText);
}
