package com.sg.dayamed.crypto;

/**
 * Created By Gorantla, Eresh on 23/Nov/2019
 **/

public abstract class ProviderBaseClass {
	protected abstract void init();

	public abstract ClearText decrypt(EncryptedText encryptedText);

	public abstract EncryptedText encrypt(ClearText clearText);
}
