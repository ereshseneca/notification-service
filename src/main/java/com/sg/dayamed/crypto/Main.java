package com.sg.dayamed.crypto;

import com.sg.dayamed.util.SensitiveDataMaskingUtility;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created By Gorantla, Eresh on 23/Nov/2019
 **/

public class Main {

	public static void main(String[] a) throws Exception {
		String encryptedData = SecurityUtil.encryptText("+17738394336");
		String decyptedData = SecurityUtil.decryptText(encryptedData);
		System.out.println(encryptedData);
		System.out.println(decyptedData);
		List<String> strings = Stream.of("eresh.zealous@gmail.com", "eresh.gorantla@senecaglobal.com")
		                             .collect(Collectors.toList());
		System.out.println(SensitiveDataMaskingUtility.maskEmailId(strings.stream().collect(Collectors.joining(", "))));
	}
}
