package com.sg.dayamed.crypto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created By Gorantla, Eresh on 23/Nov/2019
 **/

@Getter
@Setter
public class ClearText {
	private String text;
	private String keyName;
	private String iv;

	public ClearText() {
	}

	public ClearText(String text) {
		this.text = text;
		this.keyName = null;
		this.iv = null;
	}
}
