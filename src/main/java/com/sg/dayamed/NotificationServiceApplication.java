package com.sg.dayamed;

import com.sg.dayamed.component.SMSComponent;
import com.sg.dayamed.config.DefaultProfileUtil;
import com.sg.dayamed.service.ConfigParameterService;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.redis.RedisRepositoriesAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseAutoConfiguration;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.event.EventListener;
import org.springframework.core.env.Environment;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.net.InetAddress;

@SpringBootApplication(exclude = {RedisRepositoriesAutoConfiguration.class})
@ComponentScan(basePackages = {"com.sg.dayamed", "com.imantics.dm"})
@EntityScan("com.sg.dayamed.entity")
@EnableTransactionManagement
public class NotificationServiceApplication {

	private static final Logger logger = LoggerFactory.getLogger(NotificationServiceApplication.class);

	@Autowired
	ConfigParameterService configParameterService;

	@Autowired
	SMSComponent smsComponent;

	public static void main(String[] args) throws Exception {
		SpringApplication application = new SpringApplication(NotificationServiceApplication.class);
		DefaultProfileUtil.addDefaultProfile(application);
		Environment env = application.run(args)
		                             .getEnvironment();
		String protocol = "http";
		if (StringUtils.isNotBlank(env.getProperty("server.ssl.key-store"))) {
			protocol = "https";
		}
		logger.info("\n-------------------------------------------------------------------------\n\t" +
				            "Application '{}' is running! Access URLs:\n\t" +
				            "Local: \t\t{}://localhost:{}\n\t" +
				            "External: \t{}://{}:{}\n\t" +
				            "Profile(s): " +
				            "\t{}\n-------------------------------------------------------------------------",
		            env.getProperty("spring.application.name"),
		            protocol,
		            env.getProperty("server.port"),
		            protocol,
		            InetAddress.getLocalHost()
		                       .getHostAddress(),
		            env.getProperty("server.port"),
		            env.getActiveProfiles());
	}

	@EventListener(ApplicationReadyEvent.class)
	public void loadConfigParametersIntoRedis() throws Exception {
		configParameterService.loadConfigParametersToRedis();
		smsComponent.init();
	}

}
