package com.sg.dayamed.service;

/**
 * Created By Gorantla, Eresh on 23/Nov/2019
 **/

public interface ConfigParameterService {

	void loadConfigParametersToRedis();
}
