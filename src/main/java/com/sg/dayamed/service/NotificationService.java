package com.sg.dayamed.service;

import com.sg.dayamed.service.vo.NotificationVO;

import java.util.List;

/**
 * Created By Gorantla, Eresh on 23/Nov/2019
 **/

public interface NotificationService {

	void sendNotifications(List<NotificationVO> notifications);
}
