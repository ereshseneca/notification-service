package com.sg.dayamed.service.vo;

import com.sg.dayamed.component.NotificationTemplateUtils;
import com.sg.dayamed.rest.ws.WSNotification;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

/**
 * Created By Gorantla, Eresh on 23/Nov/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class NotificationVO {

	private String notificationType;

	private String channel;

	private String value;

	private Map<String, Object> paramMap = new HashMap<>();

	private String langKey = NotificationTemplateUtils.DEFAULT_LANGUAGE;

	public NotificationVO(WSNotification notification) {
		this.notificationType = notification.getNotificationType();
		this.channel = notification.getChannel();
		this.value = notification.getValue();
		this.paramMap = notification.getParamMap();
		this.langKey = notification.getLanguageKey();
	}
}
