package com.sg.dayamed.service.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created By Gorantla, Eresh on 23/Nov/2019
 **/

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MatchingPatternVO {
	private String key;
	private String value;
}

