package com.sg.dayamed.service.vo;

import com.sg.dayamed.component.vo.EmailMessageVO;
import com.sg.dayamed.component.vo.MessageModelVO;
import com.sg.dayamed.entity.NotificationResult;
import com.sg.dayamed.util.NotificationServiceConstants;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import org.apache.commons.lang3.exception.ExceptionUtils;

import java.time.ZonedDateTime;
import java.util.stream.Collectors;

/**
 * Created By Gorantla, Eresh on 23/Nov/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class NotificationResultVO {

	private String id;

	private String type;

	private String channel;

	private String value;

	private String result;

	private String stackTrace;

	private ZonedDateTime createdDate = ZonedDateTime.now();

	public NotificationResult toNotificationResult() {
		NotificationResult result = new NotificationResult();
		result.setStackTrace(this.stackTrace);
		result.setResult(this.result);
		result.setValue(this.value);
		result.setChannel(this.channel);
		result.setType(this.type);
		return result;
	}

	public NotificationResultVO(EmailMessageVO messageVO) {
		if (messageVO != null) {
			this.channel = messageVO.getChannel()
			                        .getChannel();
			this.result = messageVO.getException() == null ? NotificationServiceConstants.SUCCESS_STATUS : NotificationServiceConstants.FAILURE_STATUS;
			this.type = messageVO.getNotificationType();
			this.value = messageVO.getEmailIds()
			                      .stream()
			                      .collect(Collectors.joining(", "));
			if (messageVO.getException() != null) {
				this.stackTrace = ExceptionUtils.getStackTrace(messageVO.getException());
			}
		}
	}

	public NotificationResultVO(MessageModelVO messageVO) {
		if (messageVO != null) {
			this.channel = messageVO.getChannel()
			                        .getChannel();
			this.result = messageVO.getException() == null ? NotificationServiceConstants.SUCCESS_STATUS : NotificationServiceConstants.FAILURE_STATUS;
			this.type = messageVO.getNotificationType();
			this.value = messageVO.getContact();
			if (messageVO.getException() != null) {
				this.stackTrace = ExceptionUtils.getStackTrace(messageVO.getException());
			}
		}
	}
}
