package com.sg.dayamed.service.impl;

import com.sg.dayamed.exceptions.ApplicationException;
import com.sg.dayamed.repository.NotificationResultRepository;
import com.sg.dayamed.service.NotificationResultService;
import com.sg.dayamed.service.vo.NotificationResultVO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 * Created By Gorantla, Eresh on 23/Nov/2019
 **/
@Service
@Scope("prototype")
public class NotificationResultServiceImpl implements NotificationResultService {

	@Autowired
	NotificationResultRepository notificationResultRepository;

	@Override
	public void saveNotificationResult(NotificationResultVO resultVO) throws ApplicationException {
		try {
			notificationResultRepository.save(resultVO.toNotificationResult());
		} catch (Exception e) {
			throw new ApplicationException(e);
		}
	}
}
