package com.sg.dayamed.service.impl;

import com.sg.dayamed.entity.ConfigParams;
import com.sg.dayamed.exceptions.ExceptionLogUtil;
import com.sg.dayamed.redis.RedisService;
import com.sg.dayamed.repository.ConfigParamRepository;
import com.sg.dayamed.service.ConfigParameterService;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created By Gorantla, Eresh on 23/Nov/2019
 **/
@Service
public class ConfigParameterServiceImpl implements ConfigParameterService {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	ConfigParamRepository configParamRepository;

	@Autowired
	RedisService redisService;

	@Override
	public void loadConfigParametersToRedis() {
		try {
			List<ConfigParams> listConfig = configParamRepository.findAll();
			if (CollectionUtils.isNotEmpty(listConfig)) {
				listConfig.forEach(configParams -> redisService.configParamsUpdateInRedis(configParams.getKeyParam(),
				                                                                          configParams.getValueParam()));
				logger.debug("Config Parameters Loaded in to Redis server");
			}
		} catch (Exception e) {
			ExceptionLogUtil.log(e, "Exception while loading config parameters on to redis", logger);
		}
	}
}
