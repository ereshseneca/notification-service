package com.sg.dayamed.service.impl;

import com.sg.dayamed.adapter.NotificationProcessor;
import com.sg.dayamed.exceptions.ExceptionLogUtil;
import com.sg.dayamed.service.NotificationService;
import com.sg.dayamed.service.vo.NotificationVO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created By Gorantla, Eresh on 23/Nov/2019
 **/
@Service
public class NotificationServiceImpl implements NotificationService {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	NotificationProcessor notificationProcessor;

	@Override
	public void sendNotifications(List<NotificationVO> notifications) {
		try {
			notificationProcessor.sendNotification(notifications);
		} catch (Exception e) {
			ExceptionLogUtil.log(e, "Error in processing Notifications", logger);
		}
	}
}
