package com.sg.dayamed.service;

import com.sg.dayamed.exceptions.ApplicationException;
import com.sg.dayamed.service.vo.NotificationResultVO;

/**
 * Created By Gorantla, Eresh on 23/Nov/2019
 **/

public interface NotificationResultService {
	void saveNotificationResult(NotificationResultVO resultVO) throws ApplicationException;
}
