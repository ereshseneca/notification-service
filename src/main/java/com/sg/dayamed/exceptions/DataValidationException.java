package com.sg.dayamed.exceptions;

import lombok.Getter;
import lombok.Setter;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Created by Eresh Gorantla on 12/Jun/2019
 **/

@Getter
@Setter
public class DataValidationException extends ChainedException {

	private static final long serialVersionUID = 4958986506924891491L;

	private String fieldName;

	private Object[] errorKeyParameters;

	private String message;

	public DataValidationException(String errorKey) {
		this(null, errorKey);
	}

	public DataValidationException(String fieldName, String errorKey) {
		this(fieldName, errorKey, null);
	}

	public DataValidationException(String fieldName, String errorKey, Object[] errorKeyParameters) {
		this(null, null, fieldName, errorKey, errorKeyParameters);
	}

	public DataValidationException(String message, Throwable e, String fieldName, String errorKey,
	                               Object[] errorKeyParameters) {
		super(message, e, errorKey);
		this.fieldName = fieldName;
		this.errorKeyParameters = errorKeyParameters;
	}

	public String getFieldName() {
		return fieldName;
	}

	public Object[] getErrorKeyParameters() {
		return errorKeyParameters;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
				.appendSuper(super.toString())
				.append("fieldName", fieldName)
				.append("errorKey", getErrorKey())
				.append("errorKeyParameters", errorKeyParameters)
				.toString();
	}

}
