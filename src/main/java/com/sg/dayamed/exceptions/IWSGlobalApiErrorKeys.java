package com.sg.dayamed.exceptions;

/**
 * Created by Eresh Gorantla on 12/Jun/2019
 **/

public interface IWSGlobalApiErrorKeys extends IGlobalErrorKeys {

	String ERRORS_AUTHENTICATION_FAILED = "errors.authentication.failed";
	String ERRORS_PUBLIC_RECORD_NOT_FOUND = "errors.general.record.not.found";
	String ERRORS_GENERAL_RECORD_NOT_FOUND = "errors.exception.record.not.found";
	String ERRORS_AUTHORIZATION_FAILED = "errors.authorization.failed";
	String ERRORS_NO_PREVIEGES = "errors.no.privileges.to.perform";
	String ERRORS_INACTIVE = "errors.inactive.state";
	String ERRORS_ALREADY_EXISTS = "errors.already.exists";
	String ERRORS_MISSING_REQUEST_PARAMETER_FIELD = "errors.missing.request.parameter";
}
