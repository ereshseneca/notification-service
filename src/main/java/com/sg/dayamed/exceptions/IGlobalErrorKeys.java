package com.sg.dayamed.exceptions;

/**
 * Created by Eresh Gorantla on 12/Jun/2019
 **/

public interface IGlobalErrorKeys {

	String ERRORS_GENERAL_ERROR_UNEXPECTED = "errors.general.error.unexpected";
	String ERROR_UNEXPECTED_EXCEPTION = "error.unexpected.exception.occurred";
	String ERRORS_GENERAL_VALUE_REQUIRED = "errors.general.value.required";
	String ERRORS_GENERAL_VALUE_TOOLARGE = "errors.general.value.toolarge";
	String ERRORS_GENERAL_VALUE_TOOLONG = "errors.general.value.toolong";
	String ERRORS_GENERAL_VALUE_TOOSMALL = "errors.general.value.toosmall";
	String ERRORS_GENERAL_VALUE_UNEXPECTED = "errors.general.value.unexpected";
	String ERRORS_GENERAL_NUMERICS_ONLY = "errors.general.numerics.only";
	String ERRORS_GENERAL_SERVICE_UNAVAILABLE = "errors.general.service.unavailable";
	String ERRORS_GENERAL_VALUE_ALREADY_EXISTS = "errors.general.already.exists";
	String ERRORS_GENERAL_VALUE_ACCESS_DENIED = "errors.general.access.denied";
	String ERRORS_USER_NO_LONGER_IN_SYSTEM = "errors.user.no.longer.in.system";
}
